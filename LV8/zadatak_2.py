import numpy as np
from tensorflow import keras
from keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
import seaborn as sns
from keras . models import load_model

model = load_model ( 'mnist_model.keras')
model.summary()
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()
# Skaliranje slika na raspon [0, 1]
x_test = x_test.astype("float32") / 255

# Predikcije mreže na testnom skupu podataka
predictions = model.predict(x_test)

# Pronalaženje loše klasificiranih slika
misclassified_indices = np.where(np.argmax(predictions, axis=1) != y_test)[0]

# Prikaz nekoliko loše klasificiranih slika
num_samples_to_display = 5
for i in range(num_samples_to_display):
    misclassified_index = misclassified_indices[i]
    true_label = y_test[misclassified_index]
    predicted_label = np.argmax(predictions[misclassified_index])
    misclassified_image = x_test[misclassified_index]

    plt.imshow(misclassified_image, cmap='gray')
    plt.title(f'True Label: {true_label}, Predicted Label: {predicted_label}')
    plt.axis('off')
    plt.show()