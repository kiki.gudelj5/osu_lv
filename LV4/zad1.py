import pandas as pd
from sklearn.model_selection import train_test_split
import sklearn . linear_model as lm
import numpy as np
import matplotlib . pyplot as plt
from sklearn . preprocessing import MinMaxScaler
from sklearn.metrics import mean_absolute_error, mean_squared_error, mean_absolute_percentage_error

data = pd.read_csv('data_C02_emission.csv')

input_variables = ['Fuel Consumption City (L/100km)', 
                    'Fuel Consumption Hwy (L/100km)',
                    'Fuel Consumption Comb (L/100km)',
                    'Fuel Consumption Comb (mpg)',
                    'Engine Size (L)',
                    'Cylinders']

output_variable = 'CO2 Emissions (g/km)'

X = data[input_variables]
y = data[output_variable]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)

plt.scatter(X_train['Fuel Consumption City (L/100km)'], y_train, color='blue', label='Train data')
plt.scatter(X_test['Fuel Consumption City (L/100km)'], y_test, color='red', label='Test data')
plt.xlabel('Fuel Consumption City (L/100km)')
plt.ylabel('CO2 Emission')
plt.title('CO2 Emission vs Fuel Consumption City (L/100km)')
plt.legend()
plt.show()

plt.hist(X_train['Fuel Consumption City (L/100km)'], bins=20, color='blue', alpha=0.5, label='Before Scaling')
plt.xlabel('Fuel Consumption City (L/100km)')
plt.ylabel('Frequency')
plt.title('Histogram of Fuel Consumption City (L/100km) Before Scaling')
plt.legend()
plt.show()

sc = MinMaxScaler()
X_train_n = sc.fit_transform(X_train)


plt.hist(X_train_n[:, 0], bins=20, color='green', alpha=0.5, label='After Scaling')
plt.xlabel('Fuel Consumption City (L/100km) (Scaled)')
plt.ylabel('Frequency')
plt.title('Histogram of Fuel Consumption City (L/100km) After Scaling')
plt.legend()
plt.show()

sc = MinMaxScaler ()
X_train_scaled = sc.fit_transform(X_train)
X_train_scaled = pd.DataFrame(X_train_scaled, columns=X_train.columns)
X_test_scaled = sc.transform(X_test)
X_test_scaled = pd.DataFrame(X_test_scaled, columns=X_test.columns)

linearModel = lm.LinearRegression()
linearModel.fit(X_train_scaled , y_train)
print(f"Koeficijenti linearnog modela: {linearModel.coef_}")


y_test_prediction = linearModel.predict(X_test_scaled)
plt.scatter(X_test_scaled['Fuel Consumption City (L/100km)'], y_test, color="blue", label="Real values")
plt.scatter(X_test_scaled["Fuel Consumption City (L/100km)"], y_test_prediction, color="red", label="Predictions")
plt.xlabel("Fuel Consumption City (L/100km)")
plt.ylabel("C02 Emissions(g/km)")
plt.legend()
plt.show()


MSE = mean_squared_error(y_test, y_test_prediction)
print(f"Mean squared error(MSE): {MSE}")
MAE = mean_absolute_error(y_test, y_test_prediction)
print(f"Mean absolute error(MAE): {MAE}")
MAPE = mean_absolute_percentage_error(y_test, y_test_prediction)
print(f"Mean absolute percentage error(MAPE): {MAPE}")