import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, accuracy_score, recall_score, precision_score
import keras
from tensorflow import keras
from keras import layers, models

##################################################
# 1. zadatak
##################################################

# učitavanje dataseta
data = np.loadtxt('data.csv', delimiter=',',skiprows=1)
# a)
data_df = pd.DataFrame(data)
print(f'Broj mjerenja: {len(data)}')
print(f'Broj dupliciranih: {data_df.duplicated().sum()}')
print(f'Broj izostalih: {data_df.isnull().sum()} ')
data_df = data_df.drop_duplicates()
print(f'Broj mjerenja: {len(data_df)}')
data_df = data_df[data_df.iloc[:, 1] != 182.2]
print(data_df)
print(f'Broj mjerenja: {len(data_df)}')
data = data_df.to_numpy()
data_df = pd.DataFrame(data, columns=['Gender', 'Height', 'Weight'])
print(len(data_df))
print(data_df)
X = data_df[["Height", "Weight"]].to_numpy()
y = data_df["Gender"].to_numpy()
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)
plt.scatter(X_test[:,0],X_test[:,1])
plt.scatter(X_train[:,0],X_train[:,1],c='red')
plt.show()
print(len(X))