import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn . linear_model import LogisticRegression
from sklearn . metrics import confusion_matrix , ConfusionMatrixDisplay, classification_report
from sklearn . metrics import accuracy_score, precision_score, recall_score

X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

# a)
plt.scatter(X_train[:,0], X_train[:,1], label='training', cmap='coolwarm', c= y_train)
plt.scatter(X_test[:,0], X_test[:,1], label='test', cmap='coolwarm', c= y_test)
plt.xlabel('x1')
plt.ylabel('x2')


# b)
LogRegression_model = LogisticRegression ()
LogRegression_model . fit ( X_train , y_train )

y_test_p = LogRegression_model.predict( X_test )

# c)
coef = LogRegression_model.coef_[0]
intercept = LogRegression_model.intercept_
print(coef)
print(intercept)

def decision_boundary(x1):
    return (-coef[0]*x1 - intercept) / coef[1]

plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap='coolwarm')
plt.plot(X_train[:, 0], decision_boundary(X_train[:, 0]))
plt.xlabel('x1')
plt.ylabel('x2')
plt.title('Logistic Regression Decision Boundary')
plt.show()

# d)

y_pred = LogRegression_model.predict(X_test)

conf_matrix = confusion_matrix(y_test, y_pred)
print("Matrica zabune:")
print(conf_matrix)

accuracy = accuracy_score(y_test, y_pred)
print("Točnost: {:.2f}".format(accuracy))

precision = precision_score(y_test, y_pred)
print("Preciznost: {:.2f}".format(precision))

recall = recall_score(y_test, y_pred)
print("Odziv: {:.2f}".format(recall))

#e)
y_pred = LogRegression_model.predict(X_test)

correct_indices = np.where(y_pred == y_test)[0]
incorrect_indices = np.where(y_pred != y_test)[0]

plt.figure(figsize=(8, 6))
plt.scatter(X_test[correct_indices, 0], X_test[correct_indices, 1], c='green', label='Dobro klasificirani')
plt.scatter(X_test[incorrect_indices, 0], X_test[incorrect_indices, 1], c='black', label='Pogrešno klasificirani')
plt.xlabel('X1')
plt.ylabel('X2')
plt.title('Skup za testiranje s označenim dobro i pogrešno klasificiranim primjerima')
plt.legend()
plt.grid(True)
plt.show()
